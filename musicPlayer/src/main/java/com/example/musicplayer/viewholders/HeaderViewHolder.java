package com.example.musicplayer.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.musicplayer.*;

public class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView text;
    private ListsClickListener clickListener;

    public HeaderViewHolder(View view, ListsClickListener clickListener) {
        super(view);
        this.clickListener = clickListener;
        text = view.findViewById(R.id.textViewHeader);
        view.setOnClickListener(this);
    }

    public void update(String msg) {
        text.setText(msg);
    }

    @Override
    public void onClick(View view) {
        clickListener.onHeaderClick();
    }
}
