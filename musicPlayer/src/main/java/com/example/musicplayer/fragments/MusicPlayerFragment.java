package com.example.musicplayer.fragments;

import android.content.*;
import android.os.*;
import android.preference.*;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import com.example.musicplayer.*;
import com.example.musicplayer.models.*;
import com.example.musicplayer.ui.DragDropTouchListener;

public abstract class MusicPlayerFragment extends Fragment {
    protected RecyclerView recyclerView;
    protected LinearLayoutManager layoutManager;
    protected MainActivity activity;
    private FloatingActionButton floatingButton;
    protected SharedPreferences preferences;
    protected TextView emptyView;

	public abstract boolean onBackPressed(); // Return false if no action was executed
	public abstract void gotoPlayingItemPosition(PlayableItem playingItem);
	public abstract void updateListView();
    public abstract void onFloatingButtonClick();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity)getActivity();
        preferences = PreferenceManager.getDefaultSharedPreferences(activity);
    }

    public void initialize(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        floatingButton = activity.findViewById(R.id.floatingButton);
        floatingButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onFloatingButtonClick();
            }
        });
        emptyView = view.findViewById(R.id.emptyView);

        layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    public void setEmptyViewText(int text) {
        emptyView.setText(text);
    }

    public void enableSort(View view, int handler, DragDropTouchListener.OnItemMovedListener listener) {
        ImageView overlay = (ImageView) view.findViewById(R.id.imageViewOverlay);
        recyclerView.addOnItemTouchListener(new DragDropTouchListener(recyclerView, overlay, R.id.layoutHeader, handler, listener));
    }

    public void setFloatingButtonImage(int res) {
        floatingButton.setImageResource(res);
    }

    public void setFloatingButtonVisible(boolean visible) {
        floatingButton.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
