package com.example.musicplayer;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.musicplayer.viewholders.Melody;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.example.musicplayer.Utils.isWifiConnected;

public class InsertDataFirebase  extends AppCompatActivity{
    EditText songName,artistName,albumName;
    Button buttonSave;
    DatabaseReference databasereference;
    Melody melody;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firebase);

        if(isWifiConnected()){
            Toast.makeText(this,"Wi-Fi is enabled!", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this,"Enable Wi-Fi to avoid Internet Provider charges!", Toast.LENGTH_LONG).show();
        }
        songName=findViewById(R.id.songName);
        artistName=findViewById(R.id.artistName);
        albumName=findViewById(R.id.albumName);
        buttonSave=findViewById(R.id.buttonSave);
        melody=new Melody();
        databasereference= FirebaseDatabase.getInstance().getReference().child("Melody");
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                melody.setSongName(songName.getText().toString().trim());
                melody.setArtistName(artistName.getText().toString().trim());
                melody.setAlbumName(albumName.getText().toString().trim());

                databasereference.push().setValue(melody);
                Toast.makeText(InsertDataFirebase.this,"Melody added successfully",Toast.LENGTH_LONG).show();
            }
        });
    }
}
