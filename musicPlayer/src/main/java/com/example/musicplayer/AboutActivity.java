package com.example.musicplayer;

import android.content.pm.PackageManager.*;
import android.content.res.*;
import android.os.*;
import androidx.appcompat.app.*;
import android.text.Html;
import android.text.method.*;
import android.widget.*;

public class AboutActivity extends AppCompatActivity {
	private TextView textViewAbout;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);       
        setContentView(R.layout.activity_about);
        textViewAbout = findViewById(R.id.textViewAbout);
        textViewAbout.setMovementMethod(LinkMovementMethod.getInstance());
        Resources resources = getResources();
        
        String version = "";
        try {
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {}


        textViewAbout.setText(Html.fromHtml("about"));
	}
}
